#include "Square.h"

Square::Square(double a) 
{
	setA(a);
	setB(a);
}

Square::~Square() 
{
}

void Square::setA(double a) //* This function sets the value of a.
{
	this->a = a;
}

void Square::setB(double b) //* This function sets the value of b.
{
	this->b = b;
}

double Square::calculateCircumference() //* This function returns the circumference of the square.
{
	return (a + b) * 2;
}

double Square::calculateArea() //* This function returns the area of the square.
{
	return a * b;
}
