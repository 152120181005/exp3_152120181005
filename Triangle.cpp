#include "Triangle.h"

Triangle::Triangle(double A, double B, double C) 
{
	setA(A);
	setB(B);
	setC(C);
}

Triangle::~Triangle() 
{

}

void Triangle::setA(double A)  //* This function sets the value of A.
{
	a = A;
}

void Triangle::setB(double B) //* This function sets the value of B.
{
	b = B;
}

void Triangle::setC(double C) //* This function sets the value of C.
{
	c = C;
}

double Triangle::calculateCircumference() //* This function returns the circumference of the triangle.
{
	return a + b + c ;
}

