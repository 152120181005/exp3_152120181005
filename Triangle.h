#ifndef TRIANGLE_H_
#define TRIANGLE_H_

class Triangle {
public:
	Triangle(double, double, double);   //* Constructor of the triangle class
	virtual ~Triangle();   //* Destructor of the triangle class
	void setA(double);
	void setB(double);
	void setC(double);
	double calculateCircumference();
private:
	double a;
	double b;
	double c;
};

#endif /* TRIANGLE_H_ */
