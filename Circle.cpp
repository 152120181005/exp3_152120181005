#include "Circle.h"
#include<iostream>
using namespace std;

Circle::Circle(double r)
{
	setR(r);
}

Circle::~Circle()
{
}

void Circle::setR(double r)  //* This function sets the value of r.
{
	this->r = r;
}

void Circle::setR(int r)  
{
	this->r = r;
}

double Circle::getR() const  //*This function returns the radius of the circle.
{
	return r;
}

void Circle::setPI(double P) const //* To question 4
{
	P = PI;
}

double Circle::getPI() const  //* To question 4
{
	return PI;
}

double Circle::calculateCircumference()  //* This function returns the circumference of the circle.
{
	return PI * r * 2;
}

double Circle::calculateArea()  //* This function returns the area of the circle.
{
	return PI * r * r;
}

bool Circle::isEqual(const Circle& oth)  //* This function compares two circles.
{
	double r1, r2;
	r1 = r;
	r2 = oth.getR();

	if (r1 != r2)
		cout << "Circle4 and Circle5 are not equal!" << endl;
	if (r1 == r2)
		cout << "Circle4 and Circle5 are equal!" << endl;
	return true;
}