#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);  //* Constructor of the Circle class
	virtual ~Circle();  //* Destructor of the Circle class
	void setR(double);
	void setR(int);  //* To overload setR method
	double getR() const;
	void setPI(double) const;
	double getPI() const;
	double calculateCircumference();
	double calculateArea();
	bool isEqual(const Circle& oth);  //* Copy Constructor of the Circle class
private:
	double r;
	double PI = 3.14;
};
#endif /* CIRCLE_H_ */
